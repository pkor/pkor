# Introduction
Hello, I'm Petro! 👋 

I am a Backend Engineer and I currently live in [Kyiv, Ukraine](https://en.wikipedia.org/wiki/Kyiv). This README should help you learn about me and the ways I work and how best to work with me. Feel free to chat with me about `#okrsframework`, `#workwarbalance`, `#antiscience`, `#neurodiversity`, `#playbacktheatre`, and `#radicaltransparency`


Please, share a few words about our cooperation through [this anonymous google form](https://forms.gle/6EmUkdDcjicu8UNw9)
